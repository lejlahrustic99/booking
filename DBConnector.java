package sample;

import javafx.scene.control.Alert;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnector {

    public static Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/booking?allowPublicKeyRetrieval=true&useSSL=false useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&&serverTimezone=Europe/Zagreb", "ovde ono ime konekcije u sqlu","Ovde password te konekcije");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void showAlert(String title, String msg) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(msg);
        alert.showAndWait();
    }
}
